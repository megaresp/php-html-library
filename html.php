<?php

/**
 * html() library functions
 * 
 * @version     0.70.137
 * @author      Wayne Davies
 * @copyright   Wayne Davies, all rights reserved.
 * @license     https://www.apache.org/licenses/LICENSE-2.0
 */

declare(strict_types=1);

function _a ($content = '', $meta = [], bool $echo = TRUE)
{
    $name = ltrim(__FUNCTION__, '_');
    if (is_bool($meta)) { $echo = $meta; $meta = []; }
    $output = tag($name, $content, $meta, $echo);
    if ($echo) echo $output;
    else return $output;
}

function _area ($meta, bool $echo = TRUE)
{
    $output = tag_open(ltrim(__FUNCTION__, '_'), '', $meta, $echo);
    if ($echo) echo $output;
    else return $output;
}

function _article ($content = '', $meta = [], bool $echo = TRUE)
{
    $name = ltrim(__FUNCTION__, '_');
    if (is_bool($meta)) { $echo = $meta; $meta = []; }
    $output = tag($name, $content, $meta, $echo);
    if ($echo) echo $output;
    else return $output;
}

function _aside ($content = '', $meta = [], bool $echo = TRUE)
{
    $name = ltrim(__FUNCTION__, '_');
    if (is_bool($meta)) { $echo = $meta; $meta = []; }
    $output = tag($name, $content, $meta, $echo);
    if ($echo) echo $output;
    else return $output;
}

function _blockquote ($content = '', $meta = [], bool $echo = TRUE)
{
    $name = ltrim(__FUNCTION__, '_');
    if (is_bool($meta)) { $echo = $meta; $meta = []; }
    $output = tag($name, $content, $meta, $echo);
    if ($echo) echo $output;
    else return $output;
}

function _body ($content = '', bool $echo = TRUE)
{
    $meta = [];
    $name = ltrim(__FUNCTION__, '_');
    $output = tag($name, $content, $meta, $echo);
    if ($echo) echo $output;
    else return $output;
}

function _br (bool $echo = TRUE)
{
    $output = '<br>';
    if ($echo) echo $output . nl($echo);
    else return $output;
}

function _button ($content = '', $meta = [], bool $echo = TRUE)
{
    $name = ltrim(__FUNCTION__, '_');
    if (is_bool($meta)) { $echo = $meta; $meta = []; }
    $output = tag($name, $content, $meta, $echo);
    if ($echo) echo $output;
    else return $output;
}

function _code ($content = '', $meta = [], bool $echo = TRUE)
{
    $name = ltrim(__FUNCTION__, '_');
    if (is_bool($meta)) { $echo = $meta; $meta = []; }
    $output = tag($name, $content, $meta, $echo);
    if ($echo) echo $output;
    else return $output;
}

function _dd ($content = '', $meta = [], bool $echo = TRUE)
{
    $name = ltrim(__FUNCTION__, '_');
    if (is_bool($meta)) { $echo = $meta; $meta = []; }
    $output = tag($name, $content, $meta, $echo);
    if ($echo) echo $output;
    else return $output;
}

function _dl ($content = '', $meta = [], bool $echo = TRUE)
{
    $name = ltrim(__FUNCTION__, '_');
    if (is_bool($meta)) { $echo = $meta; $meta = []; }
    $output = tag($name, $content, $meta, $echo);
    if ($echo) echo $output;
    else return $output;
}

function _dt ($content = '', $meta = [], bool $echo = TRUE)
{
    $name = ltrim(__FUNCTION__, '_');
    if (is_bool($meta)) { $echo = $meta; $meta = []; }
    $output = tag($name, $content, $meta, $echo);
    if ($echo) echo $output;
    else return $output;
}

function _div ($content = '', $meta = [], bool $echo = TRUE)
{
    $name = ltrim(__FUNCTION__, '_');
    if (is_bool($meta)) { $echo = $meta; $meta = []; }
    $output = tag($name, $content, $meta, $echo);
    if ($echo) echo $output;
    else return $output;
}

function _em ($content = '', $meta = [], bool $echo = TRUE)
{
    $name = ltrim(__FUNCTION__, '_');
    if (is_bool($meta)) { $echo = $meta; $meta = []; }
    $output = tag($name, $content, $meta, $echo);
    if ($echo) echo $output;
    else return $output;
}

function _footer ($content = '', $meta = [], bool $echo = TRUE)
{
    $name = ltrim(__FUNCTION__, '_');
    if (is_bool($meta)) { $echo = $meta; $meta = []; }
    $output = tag($name, $content, $meta, $echo);
    if ($echo) echo $output;
    else return $output;
}

function _fieldset ($content = '', $meta = [], bool $echo = TRUE)
{
    $name = ltrim(__FUNCTION__, '_');
    if (is_bool($meta)) { $echo = $meta; $meta = []; }
    $output = tag($name, $content, $meta, $echo);
    if ($echo) echo $output;
    else return $output;
}

function _form ($content = '', $meta = [], bool $echo = TRUE)
{
    global $otk;

    $name = ltrim(__FUNCTION__, '_');
    if (is_bool($meta)) { $echo = $meta; $meta = []; }

    $output = tag($name, $content, $meta, $echo);
    
    if (isset($otk) && $otk instanceof WDF\Source\Otk\Otk && strrpos($output, '</form>') === FALSE)
        $output .= $otk->Field();
    
    if ($echo) echo $output;
    else return $output;
}

function _h1 ($content = '', $meta = [], bool $echo = TRUE)
{
    $name = ltrim(__FUNCTION__, '_');
    if (is_bool($meta)) { $echo = $meta; $meta = []; }
    $output = tag($name, $content, $meta, $echo);
    if ($echo) echo $output;
    else return $output;
}

function _h2 ($content = '', $meta = [], bool $echo = TRUE)
{
    $name = ltrim(__FUNCTION__, '_');
    if (is_bool($meta)) { $echo = $meta; $meta = []; }
    $output = tag($name, $content, $meta, $echo);
    if ($echo) echo $output;
    else return $output;
}

function _h3 ($content = '', $meta = [], bool $echo = TRUE)
{
    $name = ltrim(__FUNCTION__, '_');
    if (is_bool($meta)) { $echo = $meta; $meta = []; }
    $output = tag($name, $content, $meta, $echo);
    if ($echo) echo $output;
    else return $output;
}

function _h4 ($content = '', $meta = [], bool $echo = TRUE)
{
    $name = ltrim(__FUNCTION__, '_');
    if (is_bool($meta)) { $echo = $meta; $meta = []; }
    $output = tag($name, $content, $meta, $echo);
    if ($echo) echo $output;
    else return $output;
}

function _h5 ($content = '', $meta = [], bool $echo = TRUE)
{
    $name = ltrim(__FUNCTION__, '_');
    if (is_bool($meta)) { $echo = $meta; $meta = []; }
    $output = tag($name, $content, $meta, $echo);
    if ($echo) echo $output;
    else return $output;
}

function _h6 ($content = '', $meta = [], bool $echo = TRUE)
{
    $name = ltrim(__FUNCTION__, '_');
    if (is_bool($meta)) { $echo = $meta; $meta = []; }
    $output = tag($name, $content, $meta, $echo);
    if ($echo) echo $output;
    else return $output;
}

function _head ($content = '', bool $echo = TRUE)
{
    $meta = [];
    $name = ltrim(__FUNCTION__, '_');
    $output = tag($name, $content, $meta, $echo);
    if ($echo) echo $output;
    else return $output;
}

function _header ($content = '', $meta = [], bool $echo = TRUE)
{
    $name = ltrim(__FUNCTION__, '_');
    if (is_bool($meta)) { $echo = $meta; $meta = []; }
    $output = tag($name, $content, $meta, $echo);
    if ($echo) echo $output;
    else return $output;
}

function _hr ($meta = [], bool $echo = TRUE)
{
    $name = ltrim(__FUNCTION__, '_');
    if (is_bool($meta)) { $echo = $meta; $meta = []; }
    $output = tag($name, '', $meta, $echo);
    if ($echo) echo $output;
    else return $output;
}

function _html (string $lang = 'en', bool $echo = TRUE)
{
    if ($lang === '/')
    {
        $output = tag_close(ltrim(__FUNCTION__, '_'), $echo);
    }
    else if (stripos($lang, 'lang="') !== FALSE)
    {
        $lang = str_replace('lang="', '', $lang);
        $output = '<!DOCTYPE HTML>' . nl($echo) . '<html lang="' . rtrim($lang, '"') . '">' . nl($echo);
    }
    else if (is_string($lang))
    {
        $ptn = ['lang', '=', '"'];
        if (substr($lang, 0, 6) === 'lang="') $lang = str_replace($ptn, '', $lang);
        $output = '<!DOCTYPE HTML>' . nl($echo) . '<html lang="' . $lang . '">' . nl($echo);
    }
    if ($echo) echo $output;
    else return $output;
}

function _iframe ($content = '', $meta = [], bool $echo = TRUE)
{
    $name = ltrim(__FUNCTION__, '_');
    if (is_bool($meta)) { $echo = $meta; $meta = []; }
    $output = tag($name, $content, $meta, $echo);
    if ($echo) echo $output;
    else return $output;
}

function _img ($meta, bool $echo = TRUE)
{
    $output = tag_open(ltrim(__FUNCTION__, '_'), '', $meta, $echo);
    if ($echo) echo $output;
    else return $output;
}

function _input ($content = '', $meta = [], bool $echo = TRUE)
{
    $name = ltrim(__FUNCTION__, '_');
    $output = tag($name, $content, $meta, $echo);
    if ($echo) echo $output;
    else return $output;
}

function _label ($content = '', $meta = [], bool $echo = TRUE)
{
    $name = ltrim(__FUNCTION__, '_');
    if (is_bool($meta)) { $echo = $meta; $meta = []; }
    if (!empty($content) && !empty($meta))
        $output = tag_open($name, $content, $meta, $echo);
    else
        $output = tag($name, $content, $meta, $echo);
    if ($echo) echo $output;
    else return $output;
}

function _legend ($content = '', $meta = [], bool $echo = TRUE)
{
    $name = ltrim(__FUNCTION__, '_');
    if (is_bool($meta)) { $echo = $meta; $meta = []; }
    $output = tag($name, $content, $meta, $echo);
    if ($echo) echo $output;
    else return $output;
}

function _li ($content = '', $meta = [], bool $echo = TRUE)
{
    $name = ltrim(__FUNCTION__, '_');
    if (is_bool($meta)) { $echo = $meta; $meta = []; }
    $output = tag($name, $content, $meta, $echo);
    if ($echo) echo $output;
    else return $output;
}

function _link (array $meta, bool $echo = TRUE)
{
    $output = tag_open(ltrim(__FUNCTION__, '_'), '', $meta, $echo);
    if ($echo) echo $output;
    else return $output;
}

function _main ($content = '', $meta = [], bool $echo = TRUE)
{
    $name = ltrim(__FUNCTION__, '_');
    if (is_bool($meta)) { $echo = $meta; $meta = []; }
    $output = tag($name, $content, $meta, $echo);
    if ($echo) echo $output;
    else return $output;
}

function _map ($content = '', $meta = [], bool $echo = TRUE)
{
    $name = ltrim(__FUNCTION__, '_');
    if (is_bool($meta)) { $echo = $meta; $meta = []; }
    $output = tag($name, $content, $meta, $echo);
    if ($echo) echo $output;
    else return $output;
}

function _meta (array $meta, bool $echo = TRUE)
{
    $output = tag_open(ltrim(__FUNCTION__, '_'), '', $meta, $echo);
    if ($echo) echo $output;
    else return $output;
}

function _nav ($content = '', $meta = [], bool $echo = TRUE)
{
    $name = ltrim(__FUNCTION__, '_');
    if (is_bool($meta)) { $echo = $meta; $meta = []; }
    $output = tag($name, $content, $meta, $echo);
    if ($echo) echo $output;
    else return $output;
}

function _optgroup ($content = '', $meta = [], bool $echo = TRUE)
{
    $name = ltrim(__FUNCTION__, '_');
    if (is_bool($meta)) { $echo = $meta; $meta = []; }
    $output = tag($name, $content, $meta, $echo);
    if ($echo) echo $output;
    else return $output;
}

function _option ($content = '', $meta = [], bool $echo = TRUE)
{
    $name = ltrim(__FUNCTION__, '_');
    if (is_bool($meta)) { $echo = $meta; $meta = []; }
    $output = tag($name, $content, $meta, $echo);
    if ($echo) echo $output;
    else return $output;
}

function _p ($content = '', $meta = [], bool $echo = TRUE)
{
    $name = ltrim(__FUNCTION__, '_');
    if (is_bool($meta)) { $echo = $meta; $meta = []; }
    $output = tag($name, $content, $meta, $echo);
    if ($echo) echo $output;
    else return $output;
}

function _pre ($content = '', $meta = [], bool $echo = TRUE)
{
    $name = ltrim(__FUNCTION__, '_');
    if (is_bool($meta)) { $echo = $meta; $meta = []; }
    $output = tag($name, $content, $meta, $echo);
    if ($echo) echo $output;
    else return $output;
}


function _script ($content = '', bool $echo = TRUE)
{
    $output = NULL;
    $name = ltrim(__FUNCTION__, '_');

    if ($content === '/')
        $output = tag_close($name, $echo);
    else if (empty($content))
        $output = tag_open($name, $content, [], $echo);
    else if (is_array($content))
        $output = tag_selfclose($name, '', $content, $echo);
    else if (is_string($content))
        $output = tag_selfclose($name, $content, [], $echo);

    if ($echo) echo $output;
    else return $output;
}

function _section ($content = '', $meta = [], bool $echo = TRUE)
{
    $name = ltrim(__FUNCTION__, '_');
    if (is_bool($meta)) { $echo = $meta; $meta = []; }
    $output = tag($name, $content, $meta, $echo);
    if ($echo) echo $output;
    else return $output;
}

function _select ($content = '', $meta = [], bool $echo = TRUE)
{
    $name = ltrim(__FUNCTION__, '_');
    if (is_bool($meta)) { $echo = $meta; $meta = []; }
    $output = tag($name, $content, $meta, $echo);
    if ($echo) echo $output;
    else return $output;
}

function _sidebar ($content = '', $meta = [], bool $echo = TRUE)
{
    $name = ltrim(__FUNCTION__, '_');
    if (is_bool($meta)) { $echo = $meta; $meta = []; }
    $output = tag($name, $content, $meta, $echo);
    if ($echo) echo $output;
    else return $output;
}

function _source ($content = '', $meta = [], bool $echo = TRUE)
{
    $name = ltrim(__FUNCTION__, '_');
    $output = tag($name, $content, $meta, $echo);
    if ($echo) echo $output;
    else return $output;
}

function _span ($content = '', $meta = [], bool $echo = TRUE)
{
    $name = ltrim(__FUNCTION__, '_');
    if (is_bool($meta)) { $echo = $meta; $meta = []; }
    $output = tag($name, $content, $meta, $echo);
    if ($echo) echo $output;
    else return $output;
}

function _strong ($content, $meta = [], bool $echo = TRUE)
{
    $name = ltrim(__FUNCTION__, '_');
    if (is_bool($meta)) { $echo = $meta; $meta = []; }
    $output = tag($name, $content, $meta, $echo);
    if ($echo) echo $output;
    return $output;
}

function _style (string $content = '', bool $echo = TRUE)
{
    if ($content === '/')
        $output = tag_close('style', $echo);
    else if (empty($content))
        $output = tag_open('style', '', [], $echo);
    else if (is_string($content))
        $output = tag_selfclose('style', $content, [], $echo);

    if ($echo) echo $output;
    else return $output;
}

function _table ($content = '', $meta = [], bool $echo = TRUE)
{
    $name = ltrim(__FUNCTION__, '_');
    if (is_bool($meta)) { $echo = $meta; $meta = []; }
    $output = tag($name, $content, $meta, $echo);
    if ($echo) echo $output;
    else return $output;
}

function _tbody ($content = '', $meta = [], bool $echo = TRUE)
{
    $name = ltrim(__FUNCTION__, '_');
    if (is_bool($meta)) { $echo = $meta; $meta = []; }
    $output = tag($name, $content, $meta, $echo);
    if ($echo) echo $output;
    else return $output;
}

function _td ($content = '', $meta = [], bool $echo = TRUE)
{
    $name = ltrim(__FUNCTION__, '_');
    if (is_bool($meta)) { $echo = $meta; $meta = []; }
    $output = tag($name, $content, $meta, $echo);
    if ($echo) echo $output;
    return $output;
}

function _text (string $content, bool $echo = TRUE)
{
    if ($echo) echo $content . nl($echo);
    else return $content;
}

function _textarea ($content = '', $meta = [], bool $echo = TRUE)
{
    $name = ltrim(__FUNCTION__, '_');
    if (is_bool($meta)) { $echo = $meta; $meta = []; }
    $output = tag($name, $content, $meta, $echo);
    if ($echo) echo $output;
    else return $output;
}

function _tfoot ($content = '', $meta = [], bool $echo = TRUE)
{
    $name = ltrim(__FUNCTION__, '_');
    if (is_bool($meta)) { $echo = $meta; $meta = []; }
    $output = tag($name, $content, $meta, $echo);
    if ($echo) echo $output;
    else return $output;
}

function _th ($content = '', $meta = [], bool $echo = TRUE)
{
    $name = ltrim(__FUNCTION__, '_');
    if (is_bool($meta)) { $echo = $meta; $meta = []; }
    $output = tag($name, $content, $meta, $echo);
    if ($echo) echo $output;
    return $output;
}

function _thead ($content = '', $meta = [], bool $echo = TRUE)
{
    $name = ltrim(__FUNCTION__, '_');
    if (is_bool($meta)) { $echo = $meta; $meta = []; }
    $output = tag($name, $content, $meta, $echo);
    if ($echo) echo $output;
    else return $output;
}

/**
 * Echos or returns an HTML title tagset
 *
 * @param string $title
 * @param bool $echo
 * @return string
 */
function _title (string $title, bool $echo = TRUE)
{
    $output = tag_selfclose(ltrim(__FUNCTION__, '_'), $title, [], $echo);
    if ($echo) echo $output;
    else return $output;
}

/**
 * Echos or returns a ul tag (opening, closing or both) depending on settings
 *
 * @param string $content
 * @param array $meta
 * @param bool $echo
 * @return string
 */
function _tr ($content = '', $meta = [], bool $echo = TRUE)
{
    $name = ltrim(__FUNCTION__, '_');
    if (is_bool($meta)) { $echo = $meta; $meta = []; }
    $output = tag($name, $content, $meta, $echo);
    if ($echo) echo $output;
    else return $output;
}

/**
 * Echos or returns a <u> tag (opening, closing or both) depending on settings
 *
 * @param string $content
 * @param array $meta
 * @param bool $echo
 * @return string
 */
function _u ($content = '', $meta = [], bool $echo = TRUE)
{
    $name = ltrim(__FUNCTION__, '_');
    if (is_bool($meta)) { $echo = $meta; $meta = []; }
    $output = tag($name, $content, $meta, $echo);
    if ($echo) echo $output;
    else return $output;
}

/**
 * Echos or returns a ul tag (opening, closing or both) depending on settings
 *
 * @param string $content
 * @param array $meta
 * @param bool $echo
 * @return string
 */
function _ul ($content = '', $meta = [], bool $echo = TRUE)
{
    $name = ltrim(__FUNCTION__, '_');
    if (is_bool($meta)) { $echo = $meta; $meta = []; }
    $output = tag($name, $content, $meta, $echo);
    if ($echo) echo $output;
    else return $output;
}

/**
 * Echos or returns a video tag (opening, closing or both) depending on settings
 *
 * @param string $content
 * @param array $meta
 * @param bool $echo
 * @return string
 */
function _video ($content = '', $meta = [], bool $echo = TRUE)
{
    $name = ltrim(__FUNCTION__, '_');
    if (is_bool($meta)) { $echo = $meta; $meta = []; }
    $output = tag($name, $content, $meta, $echo);
    if ($echo) echo $output;
    else return $output;
}

/******************** Internal Library Functions ********************/
/**                                                                **/
/**   The following functions aren't meant to be called directly   **/
/**                                                                **/
/******************** Internal Library Functions ********************/

/**
 * Returns $data with HTML's special characters, low ASCII and high ASCII
 * characters escaped for safe display - except for \n, which is preserved
 *
 * @param ?string $data
 * @return string
 */
function clean ($data): string
{
    $ptn = PHP_EOL;
    $rpl = "{[(n)]}";
    $data = str_replace($ptn, $rpl, $data);
    $data = filter_var
    (
        htmlspecialchars
        (
            trim($data),
            ENT_QUOTES|ENT_HTML5,
            'UTF-8',
            FALSE
        ),
        FILTER_SANITIZE_STRING,
        FILTER_FLAG_STRIP_LOW|FILTER_FLAG_ENCODE_HIGH
    );
    return str_replace($rpl, $ptn, $data);
}

/**
 * Returns the End of Line character if $echo is TRUE.
 * Otherwise returns an empty string.
 *
 * @param bool $echo
 * @return string
 */
function nl (bool $echo = TRUE): string
{
    if ($echo) return PHP_EOL;
    else return '';
}

/**
 * Returns a string containing the key and value pairs in $data
 *
 * @param array $data
 * @return string
 */
function process_meta (array $data): string
{
    if (empty($data)) return '';

    $retstr = '';

    foreach ($data as $key => $val) {
        if (is_string($key)) {
            $retstr .= ' ' . $key;
            if ($val === NULL) $retstr .= '';
            else if ($val !== "0" && empty($val)) $retstr .= ' ';
            else $retstr .= '="' . $val . '"';
        }
    }

    return $retstr;
}

/**
 * Returns an HTML tag based on the supplied parameters
 *
 * @param string $name
 * @param mixed $content
 * @param mixed $meta
 * @param bool $echo
 * @return string
 */
function tag (string $name, $content, $meta, bool $echo): string
{
    if (is_array($content)) { $meta = $content; $content = ''; }

    if ($content === '/')
        $output = tag_close($name, $echo);
    else if (empty($content))
        $output = tag_open($name, $content, $meta, $echo);
    else
        $output = tag_selfclose($name, $content, $meta, $echo);

    return $output;
}

/**
 * Returns a closing tag
 *
 * @param string $name
 * @param bool $echo
 * @return string
 */
function tag_close (string $name, bool $echo): string
{
    return "</$name>" . nl($echo);
}

/**
 * Returns an opening tag
 *
 * @param string $name
 * @param string $content
 * @param array $meta
 * @param bool $echo
 * @return string
 */
function tag_open (string $name, string $content, array $meta, bool $echo): string
{
    return "<$name" . process_meta($meta) . ">" . $content . nl($echo);
}

/**
 * Returns a tag with a corresponding closing tag
 *
 * @param string $name
 * @param string $content
 * @param array $meta
 * @param bool $echo
 * @return string
 */
function tag_selfclose (string $name, string $content, array $meta, bool $echo): string
{
    return "<$name" . process_meta($meta) . ">" . $content . "</$name>" . nl($echo);
}