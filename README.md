# PHP HTML Library

The PHP HTML library is a suite of PHP functions that generate HTML markup from within PHP code.

It's purpose is to give developers a way to generate HTML markup without having to issue a closing ?>

For example, consider the following mix of HTML markup and PHP code:

```php
    <article class="$page->article->classes">
        <h1 class="<?php echo $page->article->headline->classes; ?>">
            <?php echo $page->headline->text; ?>
        </h1>
        <h4 class="<?php echo $page->article->intro->classes; ?>">
            <?php echo $page->intro->text; ?>
        </h4>
        <?php echo $page->content->text; ?>
    </article>
```

The above code is difficult to read specifically because of the awkward way PHP handles HTML. When using the PHP HTML Library, the above can be rewritten as follows:

```php
    _article(['class' => $page->article->classes]);
        _h1($page->headline->text, ['class' => $page->article->headline->classes]);
        _h4($page->intro->text, ['class' => $page->article->intro->classes]);
        echo $page->content->text;
    _article('/');
```

I personally find this much easier to read. And I think you will too, once you get your eye in.

## How to add the HTML library to your project

It couldn't be easier. Simply require the file [html.php](html.php) somewhere in your project. For example:

```php
require 'html.php';
```

## Available tags

There are 58 HTML tags available as functions within the library. The PHP function name is an exact match for its HTML tagname, but preceded with an underscore. For example:
```
    <h1>      ::  _h1()
    <button>  ::  _button()
    <form>    ::  _form()
```

## Help, my favourite HTML tag is missing!

Should you panic if your favourate HTML tag is missing? Yes, definitely. There's nothing like a good panic.

Fortunately, it's very easy to add new tags to this library. Please send me a request if you want me to add a missing tag the library proper (i.e. right here in this repo).

## Help, there's no `<doctype>` tag!

The _html() function automatically inserts an HTML doctype tag. See [demo.php](demo.php) in this repo for an example of a complete HTML document.

**Note:** The demo also includes links to both external and local CSS, a JavaScript file, and is a good starting point for creating your own PHP/HTML documents using this library.

## Self-closing Tags

Some HTML tags are self-closing. For example, the `<meta>` tag does not require a closing `</meta>` tag. The PHP library handles self-closing tags automatically.

## HTML tags that have to be closed

Most HTML tags require a corresponding closing tag: `</tagname>`

The PHP HTML Library will generate a closing tag when the single string "/" is passed to the tag. For example:

```PHP
_p('/'); // generates </p>
```

The library is also capable of automatically closing a tag when the first parameter is a string. For example:

```PHP
_p("I will self-close", ['class' => 'emphasize']);
// generates <p class="emphasize">I will self-close</p>
```

## The Demo File

There is a working [demo file](demo.php) in this repo. It contains an entire HTML document, and is an ideal place to start when using this library to create your own PHP/HTML files.

## Star me!

If you find this library useful, please award me a star. And if you do, thank you very much!