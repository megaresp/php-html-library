<?php

    /**
     * html() library demo script
     * 
     * @version     0.1.5
     * @author      Wayne Davies
     * @copyright   Copyright © 2020, Wayne Davies. All rights reserved
     * @license     https://www.apache.org/licenses/LICENSE-2.0
     */

    declare(strict_types=1);

    ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);
    
    require 'html.php';

    /*
        None of the formatting in this document will be retained,
        allowing developers to suit their own style without having
        to worry about adding to the size of the HTML sent to the
        browser.
    */

    _html('en-uk'); // This tag also inserts an HTML doctype
        _head();

            /* The library function for a tag generates its own closing tag when the first parameter is a string */
            _title("Demo file for the PHP HTML Library");
            /* Example of an HTML tag that is self-closing */
            _meta(['charset' => 'UTF-8']);
            /* HTML tag attributes are added as key => value pairs in an associative array */
            _meta(['name' => 'viewport', 'content' => 'width=device-width, initial-scale=1.0']);
            /* Load external CSS - Google fonts in this case */
            _link([
                'href' => 'https://fonts.googleapis.com/css2?family=Fira+Sans:ital,wght@0,400;0,500;0,600;0,800;1,400&display=swap',
                'rel' => 'stylesheet'
            ]);
            /* Load internal CSS */
            _link(['href' => 'css/demo.css', 'type' => 'text/css', 'rel' => 'stylesheet']);
            /* Load JavaScript */
            _script(['src' => 'js/demo.js']);

        /* Generate a closing tag by sending the function the string '/' */
        _head('/');

        _body();
            _main();

                /* First parameter is a string. The library will automatically add the closing tag. */
                _h1('The PHP HTML Library Demo');

                /* Here we add an empty space to have the closing </div> added by the library */
                _div(' ', ['id' => 'msg', 'class' => 'messages']);

                /* Here the <div> can't be self-closing so we simply add the attributes */
                _section(['class' => 'intro']);
                    _p(
                        "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut " .
                        "<em>labore et dolore</em> magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation " .
                        "ullamco laboris nisi ut aliquip ex ea commodo consequat."
                    );
                    _p(
                        "Consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. " .
                        "<strong>Labore et dolore</strong> magna aliqua ut enim ad minim veniam. Excepteur sint occaecat " .
                        "cupidatat non proident, sunt in culpa."
                    );
                    _p(
                        "Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla " .
                        "pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit " .
                        "anim id est laborum."
                    );
                    _p(
                        "Consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua." .
                        "labore et dolore magna aliqua ut enim ad minim veniam."
                    );
                _section('/'); // Closing the section tag on line 55

            _main('/'); // Closing the opening main tag
        _body('/'); // Closing the opening body tag
    _html('/'); // Closing the opening html tag